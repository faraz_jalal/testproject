package com.myapp.retofitget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder> {


    private List<ProductionCompanyData> schedules;
    private int rowLayout;
    private Context context;


    public static class ScheduleViewHolder extends RecyclerView.ViewHolder {
        TextView batchType;
        TextView branch;
        TextView instname;
        TextView course;
        TextView startdate;
        TextView enddate;
        TextView sheduledby;
        public ScheduleViewHolder(View v) {
            super(v);
            batchType = (TextView) v.findViewById(R.id.batchType);
            branch = (TextView) v.findViewById(R.id.Branch);
            instname = (TextView) v.findViewById(R.id.inst_name);
            course = (TextView) v.findViewById(R.id.course);
            startdate = (TextView) v.findViewById(R.id.startDate);
            enddate = (TextView) v.findViewById(R.id.endDate);
            sheduledby = (TextView) v.findViewById(R.id.button);
        }
    }

    public ScheduleAdapter(List<ProductionCompanyData> schedules, int rowLayout, Context context) {
        this.schedules = schedules;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public ScheduleAdapter.ScheduleViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ScheduleViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, final int position) {
        holder.branch.setText(schedules.get(position).getName());
        holder.batchType.setText(schedules.get(position).getOriginCountry());
        holder.instname.setText(schedules.get(position).getId().toString());

    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }
}